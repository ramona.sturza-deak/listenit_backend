export const categories = [
  "News",
  "Education",
  "Technology",
  "Business",
  "Health",
  "Religion",
  "Science",
  "Entertaiment",
  "Others"
];

export type categoriesTypes =
| "News"
| "Education"
| "Technology"
| "Business"
| "Health"
| "Religion"
| "Science"
| "Entertaiment"
| "Others";