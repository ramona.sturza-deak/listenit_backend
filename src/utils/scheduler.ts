import Audio from '#/models/audio';
import AutoGeneratedPlaylist from '#/models/autoGeneratedPlaylist';
import cron from 'node-cron';

const generatePlaylist = async () => {
    const result = await Audio.aggregate([
        //sort by most likes, by categories
        {$sort: {likes: -1}},
        {$sample: {size: 20}},
        {$group: {_id: "$category", audios: {$push: "$$ROOT._id"}}}
      ])
    
      result.map(async (item) => {
        await AutoGeneratedPlaylist.updateOne(
          {title: item._id},
          {$set: {items: item.audios}},
          {upsert: true} //create new one with title (if dont have one with that title already)
        )
      })
}
//cron = task scheduler for auto generated playlist
//generate playlist every 24h: 0 0 * * *
//5 fields: mins, hours, day of month, month, day of week 
//for test: every 2 secs - */2 * * * * *
cron.schedule("*/2 * * * * *", async () => {
    await generatePlaylist();
})