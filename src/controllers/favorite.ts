import Audio from "#/models/audio";
import Favorite from "#/models/favorite";
import { RequestHandler } from "express";
import { isValidObjectId } from "mongoose";

export const toggleFavorite: RequestHandler = async (req, res) => {
    
    const audioId = req.query.audioId as string;
    let status: "added" | "removed";

    if(!isValidObjectId(audioId)) return res.status(422).json({error: "Audio id is invalid!"});

    const audio = await Audio.findById(audioId);
    if(!audio) return res.status(404).json({error: "Resources not found!"});

    //audio is already in fav
    const alreadyExists = await Favorite.findOne({owner: req.user.id, items: audioId});
    if(alreadyExists) {
        //remove from old list
        await Favorite.updateOne({owner: req.user.id}, {
            $pull: {items: audioId}
        })
        status = "removed";
    } else {
        const favorite = await Favorite.findOne({owner: req.user.id});
        if(favorite) {
            //add new audio to old list
            await Favorite.updateOne({owner: req.user.id}, {
                $addToSet: {items: audioId}
            })
        } else {
            //create new fav list
            await Favorite.create({owner: req.user.id, items: [audioId]});
        }
        status = "added";
    }

    if(status === "added") {
        await Audio.findByIdAndUpdate(audioId, {
            $addToSet: {likes: req.user.id}
        })
    }

    if(status === "removed") {
        await Audio.findByIdAndUpdate(audioId, {
            $pull: {likes: req.user.id}
        })
    }

    res.json({status});
}

export const getFavorites: RequestHandler = async (req, res) => {
    const userID = req.user.id;

    const favorites = await Favorite.aggregate([
        {$match: {owner: userID}},
        {$unwind: "$items"},
        {$lookup: {from: "audios", localField: "items", foreignField: "_id", as: "audioInfo"}},
        {$unwind: "$audioInfo"},
        {$lookup: {from: "users", localField: "audioInfo.owner", foreignField: "_id", as: "ownerInfo"}},
        {$unwind: "$ownerInfo"},
        {$project: {_id: 0, id: "$audioInfo._id", title: "$audioInfo.title", about: "$audioInfo.about", category: "$audioInfo.category", file: "$audioInfo.file.url", poster: "$audioInfo.poster.url", owner: {name: "$ownerInfo.name", id: "$ownerInfo._id"}}}
    ]);

    res.json({audios: favorites});
}

export const getIsFavorite: RequestHandler = async (req, res) => {
    const audioId = req.query.audioId as string;

    if(!isValidObjectId(audioId)) return res.status(422).json({error: "Invalid audio id!"});

    const favorite = await Favorite.findOne({owner: req.user.id, items: audioId});

    res.json({result: favorite ? true : false});
}